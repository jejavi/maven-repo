### Jejavi - Maven Repo ###


How to add an artifact
======================

1 - Clone this repo

2 - cd into maven-repo

3 - execute the following (replacing the arguments by the ones that are relevant to your jar):


```
#!bash

mvn install:install-file -DgroupId=talendjob -DartifactId=[jar filename] -Dversion=[jar version] -Dfile=/path/to/artifact/[jar filename].jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
```


4 - Commit and push.

Using this repo in your project
===============================

1 - Locate your pom.xml

2 - Add to repositories segment

```
#!xml

    <repository>
        <id>jejavi-repository</id>
        <url>https://bitbucket.org/jejavi/maven-repo/raw/master/repository/</url>
    </repository>
```

3 - Add to dependencies segment. Eg: createExternalPos


```
#!xml

    <dependency>
        <groupId>talendjob</groupId>
        <artifactId>createExternalPos</artifactId>
        <version>1.0</version>
    </dependency>
```

For jjvpos-model

```
mvn install:install-file -Dfile=/home/onn/Projects/jjvpos-model.git/target/jjvpos-model-[version no].jar -DlocalRepositoryPath=./repository -Dversion=[version no] -DgroupId=com.jejavi -DartifactId=jjvpos-model -Dpackaging=jar
```
